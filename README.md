<h2>README</h2>
<h3>My name is <span>Oleg Alekseenko</span>, im <span>Backend Developer</span></h1>

###### _I love it when a task is challenging and you have to think about it_

<br>

[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=olegtemek&layout=compact&theme=dracula)](https://github.com/anuraghazra/github-readme-stats)

<h2>Projects</h2>

[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=olegtemek&repo=portfolio-go&theme=dracula)](https://github.com/olegtemek/portfolio-go)
[![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=olegtemek&repo=ot-badge&theme=dracula)](https://github.com/olegtemek/ot-badge)

<h2 >Contacts</h2>

- ✈️ &nbsp;**[Telegram](https://t.me/olegtemek)**
- 💌 &nbsp;**olegtemek@gmail.com**
